package com.reznik.controller;


import com.reznik.services.ServerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;

@Controller
@RequestMapping("/server")
public class ServerController extends AbstractController {

    @Inject
    public ServerController(ServerService serverService) {
        this.serverService = serverService;
    }

    @RequestMapping(value = "/actions/new", method = RequestMethod.GET)
    public String AddNewServer(ModelMap model) {

        serverService.FillDefaultDataBaseServer(model);
        return "server/actions/new";
    }

    @RequestMapping(value = "/actions", method = RequestMethod.GET)
    public String GetActionsWithServer(@RequestParam("serverId") int serverId, ModelMap model) {
        serverService.GetDataBaseServerById(serverId, model);
        return "server/actions";
    }

}
