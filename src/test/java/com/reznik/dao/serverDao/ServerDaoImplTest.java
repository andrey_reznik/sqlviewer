package com.reznik.dao.serverDao;

import com.reznik.dao.exceptions.DataBaseServerNotFoundException;
import com.reznik.dao.mappers.DataBaseServerMapper;
import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.namedparam.EmptySqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class ServerDaoImplTest extends AbstractRepositoryTest {

    private static Logger logger = Logger.getLogger(ServerDaoImplTest.class.getName());
    private DataBaseServer dataBaseServer = new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "");
    private ServerDAO serverDAO = null;

    static final String SQL_REMOVE_SERVERS = "truncate table dataBaseServer";
    static final String SQL_RESET_IDENTITY_COUNTER = "ALTER TABLE dataBaseServer ALTER COLUMN id RESTART WITH 1";


    @Before
    public void ClearDataBaseServerTestRepository() throws Exception {
        serverDAO = new ServerDAOImpl(dataSource);
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        namedParameterJdbcTemplate.update(SQL_REMOVE_SERVERS, EmptySqlParameterSource.INSTANCE);
        namedParameterJdbcTemplate.update(SQL_RESET_IDENTITY_COUNTER, EmptySqlParameterSource.INSTANCE);
    }


    @Test
    public void GetDataBaseServerIfServerExist() throws Exception {
        InsertTestDataBaseServerInstanceToRepository();
        DataBaseServer dataBaseServer1 = serverDAO.GetDataBaseServer(ServerType.MsSqlServer, "127.0.0.1");
        assertTrue(dataBaseServer.equals(dataBaseServer1));
    }

    @Test
    public void GetDataBaseServerById() throws Exception {
        InsertTestDataBaseServerInstanceToRepository();
        DataBaseServer dataBaseServer2 = serverDAO.GetDataBaseServer(ServerType.MsSqlServer, "127.0.0.1");
        DataBaseServer dataBaseServer1 = serverDAO.GetDataBaseServerById(dataBaseServer2.getId());
        assertTrue(dataBaseServer.equals(dataBaseServer1));
    }

    @Test(expected = DataBaseServerNotFoundException.class)
    public void RemoveDataBaseById() throws Exception {
        InsertTestDataBaseServerInstanceToRepository();
        assertTrue(serverDAO.RemoveDataBaseServer(1) == 1);
        serverDAO.GetDataBaseServerById(1);
    }

    @Test(expected = DataBaseServerNotFoundException.class)
    public void GetDataBaseServerByIdIfServerNotExist() throws Exception {
        DataBaseServer dataBaseServer1 = serverDAO.GetDataBaseServerById(1);
    }


    @Test(expected = DataBaseServerNotFoundException.class)
    public void GetDataBaseServerIfServerNotFound() throws Exception {
        serverDAO.GetDataBaseServer(ServerType.MsSqlServer, "127.0.0.1");
    }

    @Test
    public void testGetAvailableServerList() throws Exception {
        InsertTestDataBaseServerInstanceToRepository();
        List<DataBaseServer> dataBaseServers = serverDAO.GetAvailableServerList();
        assertTrue(dataBaseServers.size() == 1);
        assertTrue(dataBaseServers.contains(dataBaseServer));
    }


    @Test
    public void testSaveDataBaseServer() throws Exception {
        serverDAO.SaveDataBaseServer(dataBaseServer);
        List<DataBaseServer> dataBaseServers = namedParameterJdbcTemplate.query(ServerDAOImpl.SQL_SELECT_SERVER, new DataBaseServerMapper());
        assertTrue(dataBaseServers.size() == 1);
        assertTrue(dataBaseServers.contains(dataBaseServer));
    }

    @Test
    public void testUpdateExistingDataBase() throws Exception {
        InsertTestDataBaseServerInstanceToRepository();
        DataBaseServer dataBaseServer1 = new DataBaseServer(ServerType.MsSqlServer, ".\\SQL2012", "sa", "111");
        serverDAO.UpdateExistingDataBase(dataBaseServer1, 1);
        List<DataBaseServer> dataBaseServers = namedParameterJdbcTemplate.query(ServerDAOImpl.SQL_SELECT_SERVER, new DataBaseServerMapper());
        assertTrue(dataBaseServers.size() == 1);
        assertTrue(dataBaseServers.contains(dataBaseServer1));

    }


    private void InsertTestDataBaseServerInstanceToRepository() {

        int updated = namedParameterJdbcTemplate.update(ServerDAOImpl.SQL_INSERT_SERVER, new DataBaseServerMapper().getParamsFromDataBaseServerObject(dataBaseServer));
        if (updated == 1) {
            logger.log(Level.INFO, "Test DataBaseServerObject inserted!");
        } else {
            fail("No database object inserted!");
        }
    }


}
