package com.reznik.dao.sqlFetcherDao;

import com.reznik.model.DataBaseServer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.SQLException;


public class SqlSchemeFetcherDaoFactory {

    public SqlSchemeFetcherDao GetSqlSchemeFetcherForDataBaseServer(DataBaseServer dataBaseServer, String dataBaseName) throws ClassNotFoundException, SQLException {

        ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"beans.xml"});

        DataBaseServerParams dataBaseServerParams = (DataBaseServerParams) context.getBean(SchemeFetcherDaoClassName.GetClassNameForDataBaseServerParams(dataBaseServer.getServerType()));

        Class.forName(dataBaseServerParams.getJdbcDriverClassName());

        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(dataBaseServerParams.getJdbcUrl(dataBaseServer, dataBaseName));

        SqlSchemeFetcherDao sqlSchemeFetcherDao = (SqlSchemeFetcherDao) context.getBean(SchemeFetcherDaoClassName.GetClassNameForSchemeFetcher(dataBaseServer.getServerType()));

        sqlSchemeFetcherDao.SetDataSource(driverManagerDataSource);

        return sqlSchemeFetcherDao;
    }


}
