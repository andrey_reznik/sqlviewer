package com.reznik.controller;


import com.reznik.services.SqlFetcherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.sql.SQLException;

@Controller
@RequestMapping("/server/databases")
public class DataBaseController extends AbstractController {

    @Inject
    public DataBaseController(SqlFetcherService sqlFetcherService) {
        this.sqlFetcherService = sqlFetcherService;
    }

    @RequestMapping(value = "/tables", method = RequestMethod.GET)
    public String GetTablesForDatabase(@RequestParam("server") String serverName,
                                       @RequestParam("type") String serverType,
                                       @RequestParam("database") String dataBaseName,
                                       ModelMap model) throws SQLException, ClassNotFoundException {


        sqlFetcherService.GetTablesForDataBase(serverName, serverType, dataBaseName, model);


        return "tables/list";
    }
}
