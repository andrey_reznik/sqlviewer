package com.reznik.dao.exceptions;

public class DataBaseServerNotFoundException extends DataBaseServerException {

    private static final long serialVersionUID = 3L;

    public DataBaseServerNotFoundException(String errMsg) {
        super(errMsg);

    }


}
