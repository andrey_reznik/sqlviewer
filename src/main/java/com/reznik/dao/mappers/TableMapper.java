package com.reznik.dao.mappers;

import com.reznik.model.Table;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class TableMapper implements RowMapper<Table> {
    @Override
    public Table mapRow(ResultSet rs, int rowNum) throws SQLException {
        Table table = new Table(rs.getString(1), rs.getInt(2));
        return table;
    }
}
