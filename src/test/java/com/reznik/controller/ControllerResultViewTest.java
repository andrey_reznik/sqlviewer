package com.reznik.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.logging.Logger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration("classpath:/dispatcher-servlet.xml")
public class ControllerResultViewTest {
    private MockMvc mockMvc;

    private static Logger logger = Logger.getLogger(ControllerResultViewTest.class.getName());

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    protected WebApplicationContext wac;


    @Before
    public void setup() {
        this.mockMvc = webAppContextSetup(this.wac).build();
    }

    @Test
    public void HomeControllerShouldReturnHomeView() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("home"));

    }

    @Test
    public void ServerControllerShouldReturnErrorIfServerNotFound() throws Exception {
        mockMvc.perform(get("/server/actions/databases?server=255.255.255.0&type=MsSqlServer"))
                .andExpect(status().isOk())
                .andExpect(view().name("errors/error"));
    }

    @Test
    public void ServerControllerShouldReturnToHomePageIfNewServerWasAdded() throws Exception {
        mockMvc.perform(post("/server/actions/new").param("serverType", "MsSqlServer")
                .param("serverName", ".\\SQL2012")
                .param("login", "sa")
                .param("password", ""))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));
    }


    @Test
    public void ServerControllerShouldReturnToNewPageIfPostDataIsInvalid() throws Exception {
        mockMvc.perform(post("/server/actions/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("server/actions/new"));
    }

    @Test
    public void ServerControllerShouldReturnToEditPageIfPostDataIsInvalid() throws Exception {
        mockMvc.perform(post("/server/actions/edit"))
                .andExpect(status().isOk())
                .andExpect(view().name("server/actions/edit"));
    }

    @Test
    public void ServerControllerShouldReturnEditPageWhenAddServerButtonClicked() throws Exception {
        mockMvc.perform(get("/server/actions/new"))
                .andExpect(status().isOk())
                .andExpect(view().name("server/actions/new"));

    }

    @Test
    public void ServerControllerShouldReturnActionsPageWhenClickOnServerName() throws Exception {
        mockMvc.perform(get("/server/actions").param("serverId", "3"))
                .andExpect(status().isOk())
                .andExpect(view().name("server/actions"));

    }

    @Test
    public void ServerControllerShouldReturnErrorPageWhenServerNotFound() throws Exception {
        mockMvc.perform(get("/server/actions").param("serverId", "100"))
                .andExpect(status().isOk())
                .andExpect(view().name("errors/error"));

    }

    @Test
    public void ServerControllerShouldReturnHomePageWhenServerRemovedSuccessful() throws Exception {
        mockMvc.perform(get("/server/actions/remove").param("serverId", "1"))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/"));

    }

    @Test
    public void ServerControllerShouldReturnErrorPageWhenServerNotFoundWhileRemoved() throws Exception {
        mockMvc.perform(get("/server/actions/remove").param("serverId", "100"))
                .andExpect(status().isOk())
                .andExpect(view().name("errors/error"));

    }

    @Test
    public void ServerActionControllerEditPageOnEditAction() throws Exception {
        mockMvc.perform(get("/server/actions/edit").param("serverId", "2"))
                .andExpect(status().isOk())
                .andExpect(view().name("server/actions/edit"));

    }


}
