<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <div class="body">
            <form method="get">
                <spring:url var="newServerUrl" value="/server/actions/new"/>
                <a href=${newServerUrl}>
                    <button type=button>Add new server</button>
                </a>
            </form>

            <table>
                <tbody>
                <p>
                    <tr>
                        <th>ID</th>
                        <th>Server type</th>
                        <th>Server name</th>
                    </tr>
                    <c:forEach items="${servers}" var="server">
                    <tr>
                        <td>
                <p><c:out value="${server.id}"></c:out></td>
                <td><p><c:out value="${server.serverType}"></c:out></td>
                <td><p><a href="<c:url value="/server/actions?serverId=${server.id}"> </c:url>"> <c:out
                        value="${server.serverName}"></c:out></a></td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>


    </tiles:putAttribute>
</tiles:insertDefinition>