package com.reznik.dao.exceptions;


public class DatabaseServerExistException extends DataBaseServerException {
    private static final long serialVersionUID = 2L;

    public DatabaseServerExistException(String errMsg) {
        super(errMsg);
    }
}
