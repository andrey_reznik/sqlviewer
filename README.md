Description: 
Web application, that shows databases, tables, columns with field types and field data in selected database server. 
Main tasks: 
 -Add new database server connection settings (MsSql, Postgres) 
 -Connect to selected database server and show it database list 
 -Show selected database tables 
 -Show selected table columns with data and column type 