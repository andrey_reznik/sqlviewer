package com.reznik.dao.sqlFetcherDao;

import com.reznik.model.ServerType;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SchemeFetcherDaoClassNameTest {


    @Test
    public void testGetClassNameForSchemeFetcher() throws Exception {
        String className = ServerType.MsSqlServer.toString() + SchemeFetcherDaoClassName.FETCHER_CLASS_NAME_PART;
        String gettingClassName = SchemeFetcherDaoClassName.GetClassNameForSchemeFetcher(ServerType.MsSqlServer);
        assertTrue(className.equals(gettingClassName));

    }

    @Test
    public void testGetClassNameForDataBaseServerParams() throws Exception {
        String className = ServerType.MsSqlServer.toString() + SchemeFetcherDaoClassName.DATA_BASE_SERVER_PARAMS_CLASS_NAME_PART;
        String paramsClassName = SchemeFetcherDaoClassName.GetClassNameForDataBaseServerParams(ServerType.MsSqlServer);
        assertTrue(className.equals(paramsClassName));
    }
}
