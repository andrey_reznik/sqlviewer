package com.reznik.services;

import com.reznik.dao.serverDao.ServerDAO;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDao;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDaoFactory;
import com.reznik.model.Column;
import com.reznik.model.DataBaseServer;
import com.reznik.model.Database;
import com.reznik.model.Table;
import org.springframework.ui.ModelMap;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.List;

import static com.reznik.model.ServerType.GetEnumFromString;


public class SqlFetcherServiceImpl implements SqlFetcherService {

    private SqlSchemeFetcherDaoFactory sqlSchemeFetcherDaoFactory;
    private ServerDAO serverDAO;

    private DataBaseServer dataBaseServer;
    private SqlSchemeFetcherDao sqlSchemeFetcherDao;


    @Inject
    public SqlFetcherServiceImpl(SqlSchemeFetcherDaoFactory sqlSchemeFetcherDaoFactory, ServerDAO serverDAO) {
        this.sqlSchemeFetcherDaoFactory = sqlSchemeFetcherDaoFactory;
        this.serverDAO = serverDAO;
    }


    @Override
    public void GetDataBaseList(String serverName, String serverType, ModelMap modelMap) throws SQLException, ClassNotFoundException {

        FillLocalDataBaseServerAndSqlFetcher(serverName, serverType, Database.NoDbName);

        List<Database> databases = sqlSchemeFetcherDao.GetDatabases(dataBaseServer);

        modelMap.put("databases", databases);
        modelMap.put("dataBaseServer", dataBaseServer);
    }


    @Override
    public void GetColumnsOfTablesForDataBase(String serverName, String serverType, String dataBaseName, String tableName, ModelMap modelMap) throws SQLException, ClassNotFoundException {

        FillLocalDataBaseServerAndSqlFetcher(serverName, serverType, dataBaseName);

        List<Column> columns = sqlSchemeFetcherDao.GetColumnsForTable(dataBaseName, tableName);
        modelMap.put("databaseServer", dataBaseServer);
        modelMap.put("databaseName", dataBaseName);
        modelMap.put("tableName", tableName);
        modelMap.put("columns", columns);
    }


    @Override
    public void GetTablesForDataBase(String serverName, String serverType, String dataBaseName, ModelMap modelMap) throws SQLException, ClassNotFoundException {

        FillLocalDataBaseServerAndSqlFetcher(serverName, serverType, dataBaseName);

        List<Table> tables = sqlSchemeFetcherDao.GetTablesForDataBase(dataBaseName);

        modelMap.put("databaseServer", dataBaseServer);
        modelMap.put("databaseName", dataBaseName);
        modelMap.put("tables", tables);
    }


    private void FillLocalDataBaseServerAndSqlFetcher(String serverName, String serverType, String dataBaseName) throws SQLException, ClassNotFoundException {
        dataBaseServer = serverDAO.GetDataBaseServer(GetEnumFromString(serverType), serverName);
        sqlSchemeFetcherDao = sqlSchemeFetcherDaoFactory.GetSqlSchemeFetcherForDataBaseServer(dataBaseServer, dataBaseName);
    }

}
