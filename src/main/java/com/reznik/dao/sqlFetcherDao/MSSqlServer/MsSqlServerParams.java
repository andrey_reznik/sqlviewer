package com.reznik.dao.sqlFetcherDao.MSSqlServer;

import com.reznik.dao.sqlFetcherDao.DataBaseServerParams;
import com.reznik.model.DataBaseServer;

public class MsSqlServerParams implements DataBaseServerParams {

    static final String MSSQL_JDBC_CLASS_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    static final String MSSQL_JDBC_URL = "jdbc:sqlserver://%s;user=%s;password=%s";

    @Override
    public String getJdbcDriverClassName() {
        return MSSQL_JDBC_CLASS_NAME;
    }

    @Override
    public String getJdbcUrl(DataBaseServer dataBaseServer, String dataBaseName) {
        return String.format(MSSQL_JDBC_URL, dataBaseServer.getServerName(), dataBaseServer.getLogin(), dataBaseServer.getPassword());
    }


}
