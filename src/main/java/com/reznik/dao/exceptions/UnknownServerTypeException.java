package com.reznik.dao.exceptions;

public class UnknownServerTypeException extends RuntimeException {

    private static final long serialVersionUID = 11L;

    private String errMsg;

    public UnknownServerTypeException(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

}
