package com.reznik.model;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class DataBaseServerTest {
    @Test
    public void TwoSqlServersWithSameNamesAreEquals() throws Exception {
        DataBaseServer dataBaseServer1 = new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "123");
        DataBaseServer dataBaseServer2 = new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "");
        assertTrue(dataBaseServer1.equals(dataBaseServer2));
    }

    @Test
    public void TwoSqlServersWithDiffNamesAreNotEquals() throws Exception {
        DataBaseServer dataBaseServer1 = new DataBaseServer(ServerType.MsSqlServer, "192.168.0.250", "sa", "123");
        DataBaseServer dataBaseServer2 = new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "123");
        assertFalse(dataBaseServer1.equals(dataBaseServer2));
    }

    @Test
    public void SameObjHasEqualsHashCodes() throws Exception {
        DataBaseServer dataBaseServer1 = new DataBaseServer(ServerType.PosgreSql, "127.0.0.1", "sa", "123");
        DataBaseServer dataBaseServer2 = new DataBaseServer(ServerType.PosgreSql, "127.0.0.1", "posgres", "");
        assertTrue(dataBaseServer1.hashCode() == dataBaseServer2.hashCode());
    }

    @Test
    public void EqualsWithNullObjectShouldBeFalse() throws Exception {
        DataBaseServer dataBaseServer = new DataBaseServer(ServerType.PosgreSql, "127.0.0.1", "sa", "123");
        Object obj = null;
        assertFalse(dataBaseServer.equals(obj));
    }
}
