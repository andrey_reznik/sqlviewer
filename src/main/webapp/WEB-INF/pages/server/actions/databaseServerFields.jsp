<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<table cellspacing="10">
    <tr>
        <th><label for="server_name">Server name:</label></th>
        <td><sf:input path="serverName" size="25" id="server_name"/><br/>
            <sf:errors path="serverName" cssClass="error"/>
        </td>
    </tr>
    <tr>
        <th><label for="server_type_values">Server type:</label></th>
        <td><sf:select path="serverType" id="server_type_values">
            <sf:options items="${serverTypes}"/>

        </sf:select>
        </td>
    </tr>
    <tr>
        <th><label for="server_login">Login:</label></th>
        <td><sf:input path="login" size="25" id="server_login"/><br/>
            <sf:errors path="login" cssClass="error"/>
        </td>
    </tr>
    <tr>
        <th><label for="server_password">Password:</label></th>
        <td><sf:password path="password" size="25" id="server_password"/><br/>
        </td>
    </tr>
    <tr>
    </tr>
</table>
