<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="body">
            <p>

            <h2>${databaseServer.serverType} on ${databaseServer.serverName}...${databaseName} </h2>
            <table>
                <tbody>
                <p>
                    <tr>
                        <th>Table name</th>
                        <th>Row count</th>
                    </tr>
                    <c:forEach items="${tables}" var="table">
                    <tr>
                            <spring:url var="tablesUrl"
                                        value="/server/databases/tables/columns?server=${databaseServer.serverName}&type=${databaseServer.serverType}&database=${databaseName}&table=${table.tableName}"/>
                        <td>
                <p><a href="${tablesUrl}">${table.tableName}</a>
                </td>
                <td>
                    <c:out value=" ${table.rowCount} rows"></c:out>
                </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </tiles:putAttribute>

    <tiles:putAttribute name="menu">
        <spring:url var="backurl"
                    value="/server/actions/databases?server=${databaseServer.serverName}&type=${databaseServer.serverType}"/>
        <div class="menu">
            <jsp:include page="../template/menu/menuLink.jsp"/>
            <a href="${backurl}">Back</a>
        </div>
    </tiles:putAttribute>

</tiles:insertDefinition>
