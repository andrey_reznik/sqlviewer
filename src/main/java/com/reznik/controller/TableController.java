package com.reznik.controller;


import com.reznik.services.SqlFetcherService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.inject.Inject;
import java.sql.SQLException;

@Controller
@RequestMapping("/server/databases/tables")
public class TableController extends AbstractController {


    @Inject
    public TableController(SqlFetcherService sqlFetcherService) {
        this.sqlFetcherService = sqlFetcherService;
    }


    @RequestMapping(value = "/columns", method = RequestMethod.GET)
    public String GetTablesForDatabase(@RequestParam("server") String serverName,
                                       @RequestParam("type") String serverType,
                                       @RequestParam("database") String dataBaseName,
                                       @RequestParam("table") String tableName,
                                       ModelMap model) throws SQLException, ClassNotFoundException {

        sqlFetcherService.GetColumnsOfTablesForDataBase(serverName, serverType, dataBaseName, tableName, model);


        return "columns/list";
    }
}
