package com.reznik.controller;


import com.reznik.controller.validator.DataBaseServerValidator;
import com.reznik.model.DataBaseServer;
import com.reznik.services.ServerService;
import com.reznik.services.SqlFetcherService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.inject.Inject;
import javax.validation.Valid;
import java.sql.SQLException;
import java.util.Map;

@Controller
@RequestMapping("/server/actions")
public class ServerActionsController extends AbstractController {

    private static Logger logger = Logger.getLogger(ServerActionsController.class.getName());

    @Inject
    public ServerActionsController(SqlFetcherService sqlFetcherService, ServerService serverService) {
        this.sqlFetcherService = sqlFetcherService;
        this.serverService = serverService;

    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.setValidator(new DataBaseServerValidator());
    }


    @RequestMapping(value = "/new", method = RequestMethod.POST)
    public String addNewServerAction(@Valid @ModelAttribute("dataBaseServer") DataBaseServer dataBaseServer,
                                     BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "server/actions/new";
        }
        serverService.AddNewDataBaseServer(dataBaseServer);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editServerAction(@Valid @ModelAttribute("dataBaseServer") DataBaseServer dataBaseServer,
                                   BindingResult bindingResult,
                                   @RequestParam Map<String, String> requestParams) {
        if (bindingResult.hasErrors()) {
            return "server/actions/edit";
        }

        serverService.UpdateExistingDataBase(dataBaseServer, requestParams.get("serverId"));

        return "redirect:/";
    }

    @RequestMapping(value = "/remove", method = RequestMethod.GET)
    public String RemoveServerAction(@RequestParam("serverId") int serverId) {
        serverService.RemoveDataBase(serverId);
        return "redirect:/";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String EditServerAction(@RequestParam("serverId") int serverId, ModelMap modelMap) {
        serverService.EditDataBase(serverId, modelMap);
        return "server/actions/edit";
    }


    @RequestMapping(value = "/databases", method = RequestMethod.GET)
    public String DataBaseListForServer(@RequestParam("server") String serverName,
                                        @RequestParam("type") String serverType, ModelMap model) throws SQLException, ClassNotFoundException {

        sqlFetcherService.GetDataBaseList(serverName, serverType, model);

        return "databases/list";
    }

}
