<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="body">
            <h2>Some error occurred:</h2>

            <p>${error}</p>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>