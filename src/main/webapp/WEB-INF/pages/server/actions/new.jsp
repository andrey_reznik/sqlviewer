<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <div class="body">
            <h2>Add new database server</h2>

            <sf:form method="POST" modelAttribute="dataBaseServer">

                <jsp:include page="databaseServerFields.jsp"/>

                <input name="commit" type="submit" value="Add new server" size="25"/>
            </sf:form>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
