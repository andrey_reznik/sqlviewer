package com.reznik.model;

public class Table {

    private String tableName;
    private int rowCount;

    public Table(String tableName, int rowCount) {
        this.tableName = tableName;
        this.rowCount = rowCount;
    }

    public String getTableName() {
        return tableName;
    }

    public int getRowCount() {
        return rowCount;
    }
}
