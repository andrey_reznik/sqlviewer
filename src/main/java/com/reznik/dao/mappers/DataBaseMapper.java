package com.reznik.dao.mappers;

import com.reznik.model.Database;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DataBaseMapper implements RowMapper<Database> {
    @Override
    public Database mapRow(ResultSet rs, int rowNum) throws SQLException {
        Database dataBase = new Database(rs.getString(1));
        return dataBase;
    }
}