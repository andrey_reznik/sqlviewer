package com.reznik.dao.mappers;

import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DataBaseServerMapper implements RowMapper<DataBaseServer> {
    @Override
    public DataBaseServer mapRow(ResultSet rs, int rowNum) throws SQLException {
        DataBaseServer dataBaseServer = new DataBaseServer();
        dataBaseServer.setId(rs.getInt(1));
        dataBaseServer.setServerType(ServerType.GetEnumFromString(rs.getString(2)));
        dataBaseServer.setServerName(rs.getString(3));
        dataBaseServer.setLogin(rs.getString(4));
        dataBaseServer.setPassword(rs.getString(5));
        return dataBaseServer;
    }

    public Map<String, Object> getParamsFromDataBaseServerObject(DataBaseServer dataBaseServer) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("serverId", dataBaseServer.getId());
        params.put("serverType", dataBaseServer.getServerType().toString());
        params.put("serverName", dataBaseServer.getServerName());
        params.put("login", dataBaseServer.getLogin());
        params.put("password", dataBaseServer.getPassword());
        return params;
    }

    public Map<String, Object> getParamsFromDataBaseServerObjectById(int serverId) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("serverId", serverId);
        return params;
    }
}
