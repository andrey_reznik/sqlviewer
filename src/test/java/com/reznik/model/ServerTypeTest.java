package com.reznik.model;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class ServerTypeTest {
    @Test
    public void testGetEnumFromString() throws Exception {
        String serverTypeStr = "MsSqlServer";
        ServerType serverType = ServerType.GetEnumFromString(serverTypeStr);
        assertTrue(serverType == ServerType.MsSqlServer);
    }

    @Test
    public void testAvailableValue() throws Exception {
        List<String> values = ServerType.GetAvailableValues();
        assertTrue(values.indexOf(ServerType.MsSqlServer.toString()) != -1);
        assertTrue(values.indexOf(ServerType.PosgreSql.toString()) != -1);
    }


}
