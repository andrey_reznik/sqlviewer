package com.reznik.dao.serverDao;

import org.junit.runner.RunWith;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.inject.Inject;
import javax.inject.Named;
import javax.sql.DataSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:dataSource/persistent-context.xml")
public abstract class AbstractRepositoryTest {

    @Named("dataSource")
    @Inject
    protected DataSource dataSource;

    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

}
