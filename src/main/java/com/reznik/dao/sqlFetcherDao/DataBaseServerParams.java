package com.reznik.dao.sqlFetcherDao;

import com.reznik.model.DataBaseServer;

public interface DataBaseServerParams {

    public String getJdbcDriverClassName();

    public String getJdbcUrl(DataBaseServer dataBaseServer, String dataBaseName);
}
