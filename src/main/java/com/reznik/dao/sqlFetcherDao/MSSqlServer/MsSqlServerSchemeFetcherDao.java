package com.reznik.dao.sqlFetcherDao.MSSqlServer;

import com.reznik.dao.mappers.DataBaseMapper;
import com.reznik.dao.mappers.TableMapper;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDao;
import com.reznik.model.Column;
import com.reznik.model.DataBaseServer;
import com.reznik.model.Database;
import com.reznik.model.Table;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MsSqlServerSchemeFetcherDao implements SqlSchemeFetcherDao {
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private DataSource dataSource;

    private static final String SQL_GET_DATABASE_LIST = "SELECT name FROM master..sysdatabases ORDER BY name";
    private static final String SQL_GET_TABLES_FOR_DATABASE = "use %s SELECT t.name AS table_name, i.rows FROM sys.tables AS t " +
            "INNER JOIN sys.sysindexes AS i ON t.object_id = i.id AND i.indid < 2" +
            "ORDER BY table_name";
    private static final String SQL_GET_TABLE_COLUMNS = "USE %s EXEC sp_columns %s";


    @Override
    public List<Database> GetDatabases(DataBaseServer dataBaseServer) {
        return namedParameterJdbcTemplate.query(SQL_GET_DATABASE_LIST, new DataBaseMapper());

    }

    @Override
    public List<Table> GetTablesForDataBase(String databaseName) {
        return namedParameterJdbcTemplate.query(String.format(SQL_GET_TABLES_FOR_DATABASE, databaseName), new TableMapper());
    }

    @Override
    public List<Column> GetColumnsForTable(String databaseName, String tableName) {
        return namedParameterJdbcTemplate.query(String.format(SQL_GET_TABLE_COLUMNS, databaseName, tableName), new RowMapper<Column>() {
            @Override
            public Column mapRow(ResultSet rs, int rowNum) throws SQLException {
                Column column = new Column(rs.getString(4), rs.getString(6), rs.getInt(8));
                return column;
            }
        });
    }

    @Override
    public void SetDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
    }

}
