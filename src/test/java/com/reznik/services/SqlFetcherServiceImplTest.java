package com.reznik.services;

import com.reznik.dao.serverDao.ServerDAO;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDao;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDaoFactory;
import com.reznik.model.ServerType;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class SqlFetcherServiceImplTest extends ServiceTestAbstract {
    private SqlFetcherService sqlFetcherService;


    @BeforeClass
    public static void InitMocks() throws Exception {
        ServiceTestAbstract.InitMocks();

        sqlSchemeFetcherDaoMock = mock(SqlSchemeFetcherDao.class);
        when(sqlSchemeFetcherDaoMock.GetDatabases(dataBaseServer1)).thenReturn(databases);
        when(sqlSchemeFetcherDaoMock.GetTablesForDataBase(databaseName)).thenReturn(tables);
        when(sqlSchemeFetcherDaoMock.GetColumnsForTable(databaseName, tableName)).thenReturn(columns);


        sqlSchemeFetcherDaoFactoryMock = mock(SqlSchemeFetcherDaoFactory.class);
        when(sqlSchemeFetcherDaoFactoryMock.GetSqlSchemeFetcherForDataBaseServer(eq(dataBaseServer1), anyString())).thenReturn(sqlSchemeFetcherDaoMock);
    }


    @Before
    public void InitBeforeEachTests() throws Exception {
        super.InitBeforeEachTests();
        serverDAOMock = mock(ServerDAO.class);
        when(serverDAOMock.GetDataBaseServer(ServerType.MsSqlServer, serverName)).thenReturn(dataBaseServer1);
        when(serverDAOMock.GetDataBaseServerById(serverId)).thenReturn(dataBaseServer1);

        sqlFetcherService = new SqlFetcherServiceImpl(sqlSchemeFetcherDaoFactoryMock, serverDAOMock);
    }

    @Test
    public void testGetDataBaseList() throws Exception {
        sqlFetcherService.GetDataBaseList(serverName, serverType, modelMap);
        assertEquals(modelMap.size(), 2);
        assertEquals(modelMap.get("dataBaseServer"), dataBaseServer1);
        assertEquals(modelMap.get("databases"), databases);
        verify(sqlSchemeFetcherDaoMock).GetDatabases(dataBaseServer1);
    }

    @Test
    public void testGetColumnsOfTablesForDataBase() throws Exception {
        sqlFetcherService.GetColumnsOfTablesForDataBase(serverName, serverType, databaseName, tableName, modelMap);
        assertEquals(modelMap.size(), 4);
        assertEquals(modelMap.get("databaseServer"), dataBaseServer1);
        assertEquals(modelMap.get("databaseName"), databaseName);
        assertEquals(modelMap.get("tableName"), tableName);
        assertEquals(modelMap.get("columns"), columns);
        verify(sqlSchemeFetcherDaoMock).GetColumnsForTable(databaseName, tableName);
    }

    @Test
    public void testGetTablesForDataBase() throws Exception {
        sqlFetcherService.GetTablesForDataBase(serverName, serverType, databaseName, modelMap);
        assertEquals(modelMap.size(), 3);
        assertEquals(modelMap.get("databaseServer"), dataBaseServer1);
        assertEquals(modelMap.get("databaseName"), databaseName);
        assertEquals(modelMap.get("tables"), tables);
        verify(sqlSchemeFetcherDaoMock).GetTablesForDataBase(databaseName);
    }
}
