package com.reznik.services;

import com.reznik.dao.serverDao.ServerDAO;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDao;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDaoFactory;
import com.reznik.model.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.ui.ModelMap;

import java.util.Arrays;
import java.util.List;

public class ServiceTestAbstract {

    protected SqlFetcherService sqlFetcherService;

    protected static SqlSchemeFetcherDaoFactory sqlSchemeFetcherDaoFactoryMock;
    protected static SqlSchemeFetcherDao sqlSchemeFetcherDaoMock;
    protected static ServerDAO serverDAOMock;
    protected static DataBaseServer dataBaseServer1;
    protected static DataBaseServer dataBaseServer2;
    protected static String serverName = ".\\SQL2008";
    protected static String serverType = ServerType.MsSqlServer.toString();

    protected static String databaseName = "Pult4DB";
    protected static String tableName = "Panel";

    protected static List<Database> databases;
    protected static List<Table> tables;
    protected static List<Column> columns;
    protected static ModelMap modelMap;
    protected static int serverId = 1;

    @BeforeClass
    public static void InitMocks() throws Exception {
        dataBaseServer1 = new DataBaseServer(ServerType.MsSqlServer, serverName, "sa", "");
        dataBaseServer2 = new DataBaseServer(ServerType.PosgreSql, "127.0.0.1", "postgres", "postgres");

        databases = Arrays.asList(new Database("master"), new Database("Pult"));
        tables = Arrays.asList(new Table("panel", 5), new Table("mphone", 1000), new Table("group", 10));
        columns = Arrays.asList(new Column("panelId", "varchar", 16), new Column("mphoneId", "int", 4));
    }

    @Before
    public void InitBeforeEachTests() throws Exception {
        modelMap = new ModelMap();
    }


}
