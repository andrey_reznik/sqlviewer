package com.reznik.model;


public class DataBaseServer {

    private int id;
    private ServerType serverType;
    private String serverName;
    private String login;
    private String password;


    public DataBaseServer(ServerType serverType, String serverName, String login, String password) {
        this.id = 0;
        this.serverType = serverType;
        this.serverName = serverName;
        this.login = login;
        this.password = password;
    }

    public DataBaseServer() {
    }

    public static DataBaseServer GetDefault() {
        return new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "");
    }


    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof DataBaseServer))
            return false;

        DataBaseServer dataBaseServer = (DataBaseServer) obj;

        return serverType.equals(dataBaseServer.getServerType()) &&
                serverName.equals(dataBaseServer.getServerName()) &&
                login.equals(dataBaseServer.getLogin());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 37 * result + serverType.hashCode();
        result = 37 * result + serverName.hashCode();
        return result;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ServerType getServerType() {
        return serverType;
    }

    public void setServerType(ServerType serverType) {
        this.serverType = serverType;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getServerName() {
        return serverName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
