package com.reznik.services;

import com.reznik.dao.exceptions.DataBaseServerNotFoundException;
import com.reznik.dao.serverDao.ServerDAO;
import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.*;


public class ServerServiceImplTest extends ServiceTestAbstract {
    private ServerService serverService;

    @Before
    public void InitBeforeEachTests() throws Exception {
        super.InitBeforeEachTests();
        serverDAOMock = mock(ServerDAO.class);
        when(serverDAOMock.GetDataBaseServer(ServerType.MsSqlServer, serverName)).thenReturn(dataBaseServer1);
        when(serverDAOMock.GetDataBaseServerById(serverId)).thenReturn(dataBaseServer1);
        serverService = new ServerServiceImpl(serverDAOMock);
    }


    @Test
    public void testAddNewDataBaseServer() throws Exception {
        serverService.AddNewDataBaseServer(dataBaseServer1);
        verify(serverDAOMock).SaveDataBaseServer(dataBaseServer1);
    }

    @Test
    public void testFillDefaultDataBaseServer() throws Exception {
        serverService.FillDefaultDataBaseServer(modelMap);
        assertEquals(modelMap.size(), 2);
        assertEquals(modelMap.get("dataBaseServer"), DataBaseServer.GetDefault());
        assertEquals(modelMap.get("serverTypes"), ServerType.GetAvailableValues());
    }

    @Test
    public void testGetAvailableServers() throws Exception {
        serverService.GetAvailableServers(modelMap);
        assertEquals(modelMap.size(), 1);
        assertTrue(modelMap.get("servers") != null);
        verify(serverDAOMock).GetAvailableServerList();
    }

    @Test
    public void testGetDataBaseServerById() throws Exception {
        serverService.GetDataBaseServerById(1, modelMap);
        assertEquals(modelMap.size(), 1);
        assertEquals(modelMap.get("server"), dataBaseServer1);
        verify(serverDAOMock).GetDataBaseServerById(1);
    }

    @Test
    public void testRemoveDataBase() throws Exception {
        serverService.RemoveDataBase(serverId);
        verify(serverDAOMock).RemoveDataBaseServer(serverId);
    }

    @Test
    public void testEditDataBaseAction() throws Exception {
        serverService.EditDataBase(1, modelMap);
        verify(serverDAOMock).GetDataBaseServerById(1);
        assertEquals(modelMap.size(), 3);
        assertEquals(modelMap.get("dataBaseServer"), dataBaseServer1);
        assertEquals(modelMap.get("serverTypes"), ServerType.GetAvailableValues());

    }

    @Test
    public void testUpdateExistingDataBaseAction() throws Exception {
        serverService.UpdateExistingDataBase(dataBaseServer2, "1");
        verify(serverDAOMock).UpdateExistingDataBase(dataBaseServer2, 1);
    }

    @Test(expected = DataBaseServerNotFoundException.class)
    public void testUpdateExistingDataBaseActionIfDataBaseIdIsInvalid() throws Exception {
        serverService.UpdateExistingDataBase(dataBaseServer2, "ab");
    }
}
