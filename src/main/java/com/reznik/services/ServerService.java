package com.reznik.services;

import com.reznik.model.DataBaseServer;
import org.springframework.ui.ModelMap;

public interface ServerService {

    public void AddNewDataBaseServer(DataBaseServer dataBaseServer);

    public void FillDefaultDataBaseServer(ModelMap modelMap);

    public void GetAvailableServers(ModelMap modelMap);

    public void GetDataBaseServerById(int serverId, ModelMap model);

    public void RemoveDataBase(int serverId);

    public void EditDataBase(int serverId, ModelMap modelMap);

    public void UpdateExistingDataBase(DataBaseServer dataBaseServer, String serverId);

}
