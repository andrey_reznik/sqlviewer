package com.reznik.dao.serverDao;

import com.reznik.dao.exceptions.DataBaseServerNotFoundException;
import com.reznik.dao.exceptions.DatabaseServerExistException;
import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;

import java.util.List;

public interface ServerDAO {

    public DataBaseServer GetDataBaseServer(ServerType serverType, String serverName) throws DataBaseServerNotFoundException;

    public List<DataBaseServer> GetAvailableServerList();

    void SaveDataBaseServer(DataBaseServer dataBaseServer) throws DatabaseServerExistException;

    DataBaseServer GetDataBaseServerById(int serverId) throws DataBaseServerNotFoundException;

    int RemoveDataBaseServer(int serverId);

    void UpdateExistingDataBase(DataBaseServer dataBaseServer, int serverIdInt) throws DataBaseServerNotFoundException;
}
