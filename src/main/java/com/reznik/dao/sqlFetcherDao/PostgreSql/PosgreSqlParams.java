package com.reznik.dao.sqlFetcherDao.PostgreSql;


import com.reznik.dao.sqlFetcherDao.DataBaseServerParams;
import com.reznik.model.DataBaseServer;
import com.reznik.model.Database;

public class PosgreSqlParams implements DataBaseServerParams {

    static final String POSTGRESQL_JDBC_CLASS_NAME = "org.postgresql.Driver";
    static final String POSTGRESQL_JDBC_URL = "jdbc:postgresql://%s/%s?user=%s&password=%s";
    static final String DEFAULT_DB_NAME = "postgres";

    @Override
    public String getJdbcDriverClassName() {
        return POSTGRESQL_JDBC_CLASS_NAME;
    }

    @Override
    public String getJdbcUrl(DataBaseServer dataBaseServer, String dataBaseName) {
        if (dataBaseName == Database.NoDbName)
            dataBaseName = DEFAULT_DB_NAME;
        return String.format(POSTGRESQL_JDBC_URL, dataBaseServer.getServerName(), dataBaseName, dataBaseServer.getLogin(), dataBaseServer.getPassword());

    }
}
