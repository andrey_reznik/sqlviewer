<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="body">
            <table>
                <tbody>
                <p>
                    <tr>
                        <th>ID</th>
                        <th>Server type</th>
                        <th>Server name</th>
                    </tr>
                    <tr>
                        <td>
                <p>
                    <c:out value="${server.id}"> </c:out>
                </td>
                <td>
                    <p>
                        <c:out value="${server.serverType}"> </c:out>
                </td>
                <td>
                    <p><c:out value="${server.serverName}"></c:out>
                </td>
                </tr>
                </tbody>
            </table>

            <ul>
                <li>
                    <p>
                        <a href="<c:url value="/server/actions/databases?server=${server.serverName}&type=${server.serverType}"> </c:url>">
                            <c:out value="View databases"></c:out></a>
                </li>

                <li>
                    <p><a href="<c:url value="/server/actions/edit?serverId=${server.id}"></c:url>"> <c:out
                            value="Edit server"></c:out></a>
                </li>

                <li>
                    <p><a href="<c:url value="/server/actions/remove?serverId=${server.id}"></c:url>"> <c:out
                            value="Remove server"></c:out></a>
                </li>
            </ul>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>