DROP TABLE IF EXISTS dataBaseServer;


CREATE TABLE dataBaseServer (
  id         INTEGER IDENTITY PRIMARY KEY,
  serverType VARCHAR(20) NOT NULL,
  serverName VARCHAR(50) NOT NULL,
  login      VARCHAR(12) NOT NULL,
  password   VARCHAR(20) NULL
);
