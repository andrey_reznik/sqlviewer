package com.reznik.services;

import com.reznik.dao.exceptions.DataBaseServerNotFoundException;
import com.reznik.dao.serverDao.ServerDAO;
import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.springframework.ui.ModelMap;

import javax.inject.Inject;
import java.util.List;

public class ServerServiceImpl implements ServerService {

    private ServerDAO serverDAO;

    private DataBaseServer dataBaseServer;

    @Inject
    public ServerServiceImpl(ServerDAO serverDAO) {
        this.serverDAO = serverDAO;
    }


    @Override
    public void AddNewDataBaseServer(DataBaseServer dataBaseServer) {
        serverDAO.SaveDataBaseServer(dataBaseServer);
    }

    @Override
    public void FillDefaultDataBaseServer(ModelMap modelMap) {

        dataBaseServer = DataBaseServer.GetDefault();
        modelMap.put("dataBaseServer", dataBaseServer);
        modelMap.put("serverTypes", ServerType.GetAvailableValues());
    }

    @Override
    public void GetAvailableServers(ModelMap modelMap) {

        List<DataBaseServer> serverList = serverDAO.GetAvailableServerList();
        modelMap.addAttribute("servers", serverList);
    }

    @Override
    public void GetDataBaseServerById(int serverId, ModelMap model) {
        dataBaseServer = serverDAO.GetDataBaseServerById(serverId);
        model.put("server", dataBaseServer);
    }

    @Override
    public void RemoveDataBase(int serverId) {
        serverDAO.RemoveDataBaseServer(serverId);
    }

    @Override
    public void EditDataBase(int serverId, ModelMap modelMap) {
        dataBaseServer = serverDAO.GetDataBaseServerById(serverId);
        modelMap.put("dataBaseServer", dataBaseServer);
        modelMap.put("serverTypes", ServerType.GetAvailableValues());
        modelMap.put("isEditing", true);
    }

    @Override
    public void UpdateExistingDataBase(DataBaseServer dataBaseServer, String serverId) {
        int serverIdInt;
        try {
            serverIdInt = Integer.parseInt(serverId);
        } catch (NumberFormatException ex) {
            throw new DataBaseServerNotFoundException("No dataBase found with id = " + serverId);
        }

        serverDAO.UpdateExistingDataBase(dataBaseServer, serverIdInt);

    }


}
