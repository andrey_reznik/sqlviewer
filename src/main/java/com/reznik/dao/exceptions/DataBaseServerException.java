package com.reznik.dao.exceptions;

public class DataBaseServerException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private String errMsg;

    public DataBaseServerException(String errMsg) {
        this.errMsg = errMsg;
    }

    public String getErrMsg() {
        return errMsg;
    }

    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }
}
