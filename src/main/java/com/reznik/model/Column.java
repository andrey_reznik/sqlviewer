package com.reznik.model;

public class Column {

    private String columnName;
    private String columnType;
    private int columnLen;

    public Column(String columnName, String columnType, int columnLen) {
        this.columnName = columnName;
        this.columnType = columnType;
        this.columnLen = columnLen;
    }


    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public int getColumnLen() {
        return columnLen;
    }

    public void setColumnLen(int columnLen) {
        this.columnLen = columnLen;
    }
}
