package com.reznik.controller.validator;

import com.reznik.model.DataBaseServer;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class DataBaseServerValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {

        return DataBaseServer.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "serverName",
                "required.serverName", "Server name is required!");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "login",
                "required.login", "Login is required!");

    }

}
