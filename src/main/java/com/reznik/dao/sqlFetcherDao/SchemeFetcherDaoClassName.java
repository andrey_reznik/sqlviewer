package com.reznik.dao.sqlFetcherDao;

import com.reznik.model.ServerType;

public class SchemeFetcherDaoClassName {

    protected static final String FETCHER_CLASS_NAME_PART = "SchemeFetcherDao";
    protected static final String DATA_BASE_SERVER_PARAMS_CLASS_NAME_PART = "Params";


    public static String GetClassNameForSchemeFetcher(ServerType serverType) {
        return serverType.toString() + FETCHER_CLASS_NAME_PART;
    }

    public static String GetClassNameForDataBaseServerParams(ServerType serverType) {
        return serverType.toString() + DATA_BASE_SERVER_PARAMS_CLASS_NAME_PART;
    }

}
