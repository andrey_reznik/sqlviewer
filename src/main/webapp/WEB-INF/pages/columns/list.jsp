<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">

        <div class="body">
            <p>

            <h2>${databaseServer.serverType} on ${databaseServer.serverName}...${databaseName}.${tableName} </h2>
            <table>
                <tbody>
                <p>
                    <tr>
                        <th>Column name</th>
                        <th>Column data type</th>
                    </tr>
                    <c:forEach items="${columns}" var="column">
                    <tr>
                        <td>
                <p><c:out value="${column.columnName}"></c:out>
                </td>
                <td>
                    <p><c:out value="${column.columnType}"></c:out>
                </td>
                </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </tiles:putAttribute>

    <tiles:putAttribute name="menu">
        <spring:url var="backurl"
                    value="/server/databases/tables?server=${databaseServer.serverName}&type=${databaseServer.serverType}&database=${databaseName}"/>
        <div class="menu">
            <jsp:include page="../template/menu/menuLink.jsp"/>
            <a href="${backurl}">Back</a>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
