package com.reznik.dao.sqlFetcherDao.MSSqlServer;

import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;

public class MsSqlServerParamsTest {

    private MsSqlServerParams params;
    private DataBaseServer dataBaseServer;

    @Before
    public void Init() throws Exception {
        params = new MsSqlServerParams();
        dataBaseServer = new DataBaseServer(ServerType.MsSqlServer, "127.0.0.1", "sa", "123");
    }

    @Test
    public void testGetJdbcDriverClassName() throws Exception {

        assertTrue(params.getJdbcDriverClassName().equals(MsSqlServerParams.MSSQL_JDBC_CLASS_NAME));
    }

    @Test
    public void testGetJdbcUrl() throws Exception {

        String url = "jdbc:sqlserver://127.0.0.1;user=sa;password=123";
        assertTrue(url.equals(params.getJdbcUrl(dataBaseServer, "")));
    }
}
