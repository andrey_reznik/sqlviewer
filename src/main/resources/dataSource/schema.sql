CREATE TABLE IF NOT EXISTS dataBaseServer (
  id         INTEGER PRIMARY KEY AUTOINCREMENT,
  serverType TEXT NOT NULL,
  serverName TEXT NOT NULL,
  login      TEXT NOT NULL,
  password   TEXT,
  UNIQUE (serverType, serverName)
);

