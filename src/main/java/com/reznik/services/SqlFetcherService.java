package com.reznik.services;

import org.springframework.ui.ModelMap;

import java.sql.SQLException;

public interface SqlFetcherService {

    public void GetDataBaseList(String serverName, String serverType, ModelMap modelMap) throws SQLException, ClassNotFoundException;

    public void GetColumnsOfTablesForDataBase(String serverName, String serverType, String dataBaseName, String tableName, ModelMap modelMap) throws SQLException, ClassNotFoundException;

    public void GetTablesForDataBase(String serverName, String serverType, String dataBaseName, ModelMap modelMap) throws SQLException, ClassNotFoundException;
}
