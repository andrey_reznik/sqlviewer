package com.reznik.dao.sqlFetcherDao.PostgreSql;

import com.reznik.dao.mappers.ColumnMapper;
import com.reznik.dao.mappers.DataBaseMapper;
import com.reznik.dao.mappers.TableMapper;
import com.reznik.dao.sqlFetcherDao.SqlSchemeFetcherDao;
import com.reznik.model.Column;
import com.reznik.model.DataBaseServer;
import com.reznik.model.Database;
import com.reznik.model.Table;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;


@Component
public class PosgreSqlSchemeFetcherDao implements SqlSchemeFetcherDao {

    private static final String SQL_GET_DATABASE_LIST = "SELECT datname FROM pg_database";

    private static final String SQL_GET_TABLES_FOR_DATABASE = "SELECT relname, n_live_tup  FROM pg_stat_user_tables ";

    private static final String SQL_GET_COLUMNS_FOR_TABLE = "SELECT  column_name,data_type, case \n" +
            "WHEN data_type = 'character varying' then character_maximum_length\n" +
            "WHEN data_type = 'integer' then 4\n" +
            "WHEN data_type = 'smallint' then 2\n" +
            "WHEN data_type = 'timestamp without time zone' then 8\n" +
            "WHEN data_type = 'boolean' then 1\n" +
            "END\n" +
            "FROM information_schema.columns\n" +
            "WHERE table_schema = '%s'\n" +
            "AND table_name   = '%s'\n";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
    private DataSource dataSource;


    @Override
    public List<Database> GetDatabases(DataBaseServer dataBaseServer) {
        return namedParameterJdbcTemplate.query(SQL_GET_DATABASE_LIST, new DataBaseMapper());
    }

    @Override
    public List<Table> GetTablesForDataBase(String databaseName) {
        return namedParameterJdbcTemplate.query(String.format(SQL_GET_TABLES_FOR_DATABASE, databaseName), new TableMapper());
    }

    @Override
    public List<Column> GetColumnsForTable(String databaseName, String tableName) {
        return namedParameterJdbcTemplate.query(String.format(SQL_GET_COLUMNS_FOR_TABLE, databaseName, tableName), new ColumnMapper());
    }

    @Override
    public void SetDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
        namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource);
    }


}
