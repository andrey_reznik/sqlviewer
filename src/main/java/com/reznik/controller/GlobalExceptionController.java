package com.reznik.controller;


import com.reznik.dao.exceptions.DataBaseServerException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class GlobalExceptionController {

    @ExceptionHandler(DataBaseServerException.class)
    public ModelAndView handleCustomException(DataBaseServerException ex) {
        ModelAndView model = new ModelAndView("errors/error");
        model.addObject("error", ex.getErrMsg());
        return model;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleAllException(Exception ex) {

        ModelAndView model = new ModelAndView("errors/error");
        model.addObject("error", ex.getMessage());

        return model;

    }
}
