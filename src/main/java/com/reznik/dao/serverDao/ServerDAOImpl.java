package com.reznik.dao.serverDao;

import com.reznik.dao.exceptions.DataBaseServerNotFoundException;
import com.reznik.dao.exceptions.DatabaseServerExistException;
import com.reznik.dao.mappers.DataBaseServerMapper;
import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;


@Repository
public class ServerDAOImpl implements ServerDAO {


    static final String SQL_INSERT_SERVER =
            "insert into dataBaseServer (serverType,serverName,login,password)" +
                    "values(:serverType,:serverName,:login,:password)";

    static final String SQL_SELECT_SERVER = "select id, serverType, serverName, login, password from dataBaseServer";
    static final String SQL_SELECT_SERVER_BY_NAME_AND_TYPE = SQL_SELECT_SERVER + " where serverType = :serverType and serverName =:serverName";
    static final String SQL_WHERE_ID_STATEMENT = " where id = :serverId";
    static final String SQL_SELECT_SERVER_BY_ID = SQL_SELECT_SERVER + SQL_WHERE_ID_STATEMENT;
    static final String SQL_UPDATE_SERVER_BY_ID = "update dataBaseServer set serverType = :serverType, serverName = :serverName, login = :login, password = :password" + SQL_WHERE_ID_STATEMENT;
    static final String SQL_REMOVE_SERVERS_BY_ID = "delete from dataBaseServer " + SQL_WHERE_ID_STATEMENT;


    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private DataBaseServerMapper dataBaseServerMapper;

    public ServerDAOImpl(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
        dataBaseServerMapper = new DataBaseServerMapper();
    }


    @Override
    public DataBaseServer GetDataBaseServer(ServerType serverType, String serverName) throws DataAccessException, DataBaseServerNotFoundException {

        DataBaseServer dataBaseServer = new DataBaseServer(serverType, serverName, "", "");
        Map<String, Object> paramsFromDataBaseServerObject = dataBaseServerMapper.getParamsFromDataBaseServerObject(dataBaseServer);
        try {
            DataBaseServer dataBaseServer1 = namedParameterJdbcTemplate.queryForObject(SQL_SELECT_SERVER_BY_NAME_AND_TYPE, paramsFromDataBaseServerObject, dataBaseServerMapper);
            return dataBaseServer1;
        } catch (DataAccessException ex) {
            throw new DataBaseServerNotFoundException("DataBaseServer with type=" + serverType.toString() + " and name=" + serverName + " not found!");
        }
    }

    @Override
    public List<DataBaseServer> GetAvailableServerList() throws DataAccessException {
        return namedParameterJdbcTemplate.query(SQL_SELECT_SERVER, new DataBaseServerMapper());
    }

    @Override
    public void SaveDataBaseServer(DataBaseServer dataBaseServer) throws DataAccessException, DatabaseServerExistException {
        try {
            namedParameterJdbcTemplate.update(SQL_INSERT_SERVER, new DataBaseServerMapper().getParamsFromDataBaseServerObject(dataBaseServer));
        } catch (DataAccessException ex) {
            throw new DatabaseServerExistException("DataBase server " + dataBaseServer.getServerName() + " type=" + dataBaseServer.getServerType() + " already exists!");
        }

    }

    @Override
    public DataBaseServer GetDataBaseServerById(int serverId) throws DataBaseServerNotFoundException {
        try {
            return namedParameterJdbcTemplate.queryForObject(SQL_SELECT_SERVER_BY_ID, dataBaseServerMapper.getParamsFromDataBaseServerObjectById(serverId), dataBaseServerMapper);
        } catch (DataAccessException ex) {
            throw new DataBaseServerNotFoundException("DataBaseServer with id=" + serverId + " not found!");
        }
    }

    @Override
    public int RemoveDataBaseServer(int serverId) {
        int deletedRows = namedParameterJdbcTemplate.update(SQL_REMOVE_SERVERS_BY_ID, dataBaseServerMapper.getParamsFromDataBaseServerObjectById(serverId));
        if (deletedRows == 0) {
            throw new DataBaseServerNotFoundException("DataBaseServer with id=" + serverId + " not found for removing!");
        }
        return deletedRows;
    }

    @Override
    public void UpdateExistingDataBase(DataBaseServer dataBaseServer, int serverId) throws DataBaseServerNotFoundException {
        dataBaseServer.setId(serverId);
        Map<String, Object> paramsFromDataBaseServerObject = dataBaseServerMapper.getParamsFromDataBaseServerObject(dataBaseServer);
        int updatedRows = namedParameterJdbcTemplate.update(SQL_UPDATE_SERVER_BY_ID, paramsFromDataBaseServerObject);
        if (updatedRows != 1) {
            throw new DataBaseServerNotFoundException("DataBaseServer with id=" + serverId + " not found for updating!");
        }
    }
}
