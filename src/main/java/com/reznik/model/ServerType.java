package com.reznik.model;


import java.util.ArrayList;
import java.util.List;

public enum ServerType {
    MsSqlServer,
    PosgreSql;

    public static ServerType GetEnumFromString(String value) {

        return Enum.valueOf(ServerType.class, value);
    }


    public static List<String> GetAvailableValues() {
        List<String> values = new ArrayList<String>();
        for (ServerType serverType : ServerType.values()) {
            values.add(serverType.name());

        }
        return values;
    }


}
