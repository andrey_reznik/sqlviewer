package com.reznik.controller;

import com.reznik.services.ServerService;
import com.reznik.services.SqlFetcherService;

public abstract class AbstractController {

    protected SqlFetcherService sqlFetcherService;
    protected ServerService serverService;

}
