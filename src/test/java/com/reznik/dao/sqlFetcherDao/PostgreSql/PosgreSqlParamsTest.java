package com.reznik.dao.sqlFetcherDao.PostgreSql;

import com.reznik.model.DataBaseServer;
import com.reznik.model.ServerType;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertTrue;


public class PosgreSqlParamsTest {
    private PosgreSqlParams params;
    private DataBaseServer dataBaseServer;

    @Before
    public void Init() throws Exception {
        params = new PosgreSqlParams();
        dataBaseServer = new DataBaseServer(ServerType.PosgreSql, "192.168.0.50\\Postgres", "postgres", "postgres");
    }

    @Test
    public void testGetJdbcDriverClassName() throws Exception {
        assertTrue(params.getJdbcDriverClassName().equals(PosgreSqlParams.POSTGRESQL_JDBC_CLASS_NAME));
    }

    @Test
    public void testGetJdbcUrlWhenDataBaseNameNotEmpty() throws Exception {
        String dataBaseName = "testDb";
        String url = "jdbc:postgresql://192.168.0.50\\Postgres/testDb?user=postgres&password=postgres";
        String postgresUrl = params.getJdbcUrl(dataBaseServer, dataBaseName);
        assertTrue(url.equals(postgresUrl));
    }

    @Test
    public void testGetJdbcUrlWhenDataBaseNameIsEmpty() throws Exception {
        String dataBaseName = "";
        String url = "jdbc:postgresql://192.168.0.50\\Postgres/postgres?user=postgres&password=postgres";
        assertTrue(url.equals(params.getJdbcUrl(dataBaseServer, dataBaseName)));
    }
}
