<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>


<tiles:insertDefinition name="defaultTemplate">
    <tiles:putAttribute name="body">
        <div class="body">
            <p>

            <h2>Server: "${dataBaseServer.serverName}" type: "${dataBaseServer.serverType}"</h2>
            <table>
                <tbody>
                <p>
                    <tr>
                        <th>DataBase name</th>
                    </tr>
                    <c:forEach items="${databases}" var="database">
                    <tr>
                            <spring:url var="dataBaseUrl"
                                        value="/server/databases/tables?server=${dataBaseServer.serverName}&type=${dataBaseServer.serverType}&database=${database.databaseName}"/>
                        <td>
                <p><a href="${dataBaseUrl}">${database.databaseName}</a>
                </tr>
                </c:forEach>
                </tbody>
            </table>


        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>
