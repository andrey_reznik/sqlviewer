package com.reznik.dao.mappers;

import com.reznik.model.Column;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class ColumnMapper implements RowMapper<Column> {
    @Override
    public Column mapRow(ResultSet rs, int rowNum) throws SQLException {
        Column column = new Column(rs.getString(1), rs.getString(2), rs.getInt(3));
        return column;
    }
}
