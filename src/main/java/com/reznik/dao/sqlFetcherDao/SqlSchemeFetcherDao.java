package com.reznik.dao.sqlFetcherDao;


import com.reznik.model.Column;
import com.reznik.model.DataBaseServer;
import com.reznik.model.Database;
import com.reznik.model.Table;

import javax.sql.DataSource;
import java.util.List;

public interface SqlSchemeFetcherDao {

    public List<Database> GetDatabases(DataBaseServer dataBaseServer);

    public List<Table> GetTablesForDataBase(String databaseName);

    public List<Column> GetColumnsForTable(String databaseName, String tableName);

    public void SetDataSource(DataSource dataSource);
}
