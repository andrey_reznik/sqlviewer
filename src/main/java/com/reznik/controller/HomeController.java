package com.reznik.controller;

import com.reznik.services.ServerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.sql.SQLException;

@Controller
@RequestMapping("/")
public class HomeController extends AbstractController {

    @Inject
    public HomeController(ServerService serverService) {
        this.serverService = serverService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String GetServerList(ModelMap model) throws SQLException {

        serverService.GetAvailableServers(model);

        return "home";
    }

}